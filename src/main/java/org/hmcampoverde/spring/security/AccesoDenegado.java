package org.hmcampoverde.spring.security;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Component;

/**
 * @author Ben Simpson <ben.simpson@icesoft.com> Date: 2/28/11 Time: 6:17 PM
 */
@Component
public class AccesoDenegado implements AccessDeniedHandler {

	private boolean contextRelative;
	private String errorPage = "/error/acceso.xhtml";

	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException accessDeniedException) throws IOException, ServletException {
		String redirectUrl = calculateRedirectUrl(request.getContextPath(), errorPage);
		redirectUrl = response.encodeRedirectURL(redirectUrl);
		String ajaxHeader = ((HttpServletRequest) request).getHeader("X-Requested-With");
		if ("XMLHttpRequest".equals(ajaxHeader)) {
			String ajaxRedirectXml = "<?xml version='1.0' encoding='UTF-8'?>" + "	<partial-response>"
					+ "		<redirect url='" + redirectUrl + "'/>" + "	</partial-response>";
			response.setContentType("text/xml");
			response.getWriter().write(ajaxRedirectXml);
		} else {
			response.sendRedirect(redirectUrl);
		}
	}

	protected String calculateRedirectUrl(String contextPath, String url) {
		if (!UrlUtils.isAbsoluteUrl(url)) {
			if (isContextRelative()) {
				return url;
			} else {
				return contextPath + url;
			}
		}
		if (!isContextRelative()) {
			return url;
		}
		url = url.substring(url.lastIndexOf("://") + 3);
		url = url.substring(url.indexOf(contextPath) + contextPath.length());
		if (url.length() > 1 && url.charAt(0) == '/') {
			url = url.substring(1);
		}
		return url;
	}

	public void setContextRelative(boolean useRelativeContext) {
		this.contextRelative = useRelativeContext;
	}

	protected boolean isContextRelative() {
		return contextRelative;
	}

	public String getErrorPage() {
		return errorPage;
	}

	public void setErrorPage(String errorPage) {
		this.errorPage = errorPage;
	}
}
