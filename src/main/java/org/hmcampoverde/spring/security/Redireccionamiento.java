package org.hmcampoverde.spring.security;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.util.UrlUtils;
import org.springframework.stereotype.Component;

/**
 * @author Ben Simpson <ben.simpson@icesoft.com.com> Date: 2/9/11 Time: 2:51 PM
 */
@Component
public class Redireccionamiento implements RedirectStrategy {

	private boolean contextRelative;

	public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url) throws IOException {
		String redirectUrl = calculateRedirectUrl(request.getContextPath(), url);
		redirectUrl = response.encodeRedirectURL(redirectUrl);

		boolean ajaxRedirect = request.getHeader("faces-request") != null
				&& request.getHeader("faces-request").toLowerCase().indexOf("ajax") > -1;

		if (ajaxRedirect) {
			String ajaxRedirectXml = "<?xml version='1.0' encoding='UTF-8'?>" + "	<partial-response>"
					+ "		<redirect url='" + redirectUrl + "'/>" + "	</partial-response>";
			response.setContentType("text/xml");
			response.getWriter().write(ajaxRedirectXml);
		} else {
			response.sendRedirect(redirectUrl);
		}
	}

	private String calculateRedirectUrl(String contextPath, String url) {
		if (!UrlUtils.isAbsoluteUrl(url)) {
			if (contextRelative) {
				return url;
			} else {
				return contextPath + url;
			}
		}

		if (!contextRelative) {
			return url;
		}

		url = url.substring(url.indexOf("://") + 3);
		url = url.substring(url.indexOf(contextPath) + contextPath.length());

		if (url.length() > 1 && url.charAt(0) == '/') {
			url = url.substring(1);
		}

		return url;
	}

	public void setContextRelative(boolean useRelativeContext) {
		this.contextRelative = useRelativeContext;
	}
}
