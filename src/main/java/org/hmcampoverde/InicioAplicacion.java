package org.hmcampoverde;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InicioAplicacion {

	public static void main(String[] args) {
		SpringApplication.run(InicioAplicacion.class, args);
	}

}
