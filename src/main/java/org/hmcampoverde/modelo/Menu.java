package org.hmcampoverde.modelo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Entity
@Table(name = "tbl_menu")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString(callSuper = false, onlyExplicitlyIncluded = true)
public class Menu extends Entidad {

	private static final long serialVersionUID = -1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_menu", nullable = false, length = 2)
	@EqualsAndHashCode.Include
	private Integer idMenu;

	@Column(name = "nombre_menu", nullable = false, length = 75)
	@ToString.Include
	private String nombreMenu;

	@Column(name = "url_menu", nullable = true, columnDefinition = "TEXT")
	private String urlMenu;

	@Column(name = "icono_menu", nullable = true, length = 30)
	private String iconoMenu;

	@ManyToOne
	@JoinColumn(name = "id_submenu", foreignKey = @ForeignKey(name = "FK_MENU_SUBMENU"), nullable = true)
	private Menu submenu;

	@ManyToMany(mappedBy = "menus")
	Set<Rol> roles;
}
