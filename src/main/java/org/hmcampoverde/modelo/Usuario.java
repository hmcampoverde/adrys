package org.hmcampoverde.modelo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Entity
@Table(name = "tbl_usuario")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString(callSuper = false, onlyExplicitlyIncluded = true)
public class Usuario extends Entidad {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario", length = 3, nullable = false, unique = true)
	@EqualsAndHashCode.Include
	private Integer idUsuario;

	@Column(name = "nombre_usuario", length = 75)
	private String nombreUsuario;

	@Column(name = "descripcion_usuario", length = 100)
	@ToString.Include
	private String descripcionUsuario;

	@Column(name = "clave_usuario", columnDefinition = "TEXT")
	private String claveUsuario;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "tbl_usuario_rol", joinColumns = @JoinColumn(name = "id_usuario"), inverseJoinColumns = @JoinColumn(name = "id_rol"), foreignKey = @ForeignKey(name = "FK_USUARIO_ROL"), inverseForeignKey = @ForeignKey(name = "FK_ROL_USUARIO"))
	private Set<Rol> roles;

}
