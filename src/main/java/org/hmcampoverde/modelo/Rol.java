package org.hmcampoverde.modelo;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Entity
@Table(name = "tbl_rol")
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString(callSuper = false, onlyExplicitlyIncluded = true)
public class Rol extends Entidad {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rol", length = 3, nullable = false, unique = true)
	@EqualsAndHashCode.Include
	private Integer idRol;

	@Column(name = "nombre_rol", length = 50)
	@ToString.Include
	private String nombreRol;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "tbl_rol_menu", joinColumns = @JoinColumn(name = "id_rol"), inverseJoinColumns = @JoinColumn(name = "id_menu"), foreignKey = @ForeignKey(name = "FK_ROL_MENU"), inverseForeignKey = @ForeignKey(name = "FK_MENU_ROL"))
	private Set<Menu> menus;

}
