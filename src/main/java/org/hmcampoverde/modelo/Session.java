package org.hmcampoverde.modelo;

import java.util.Collection;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @version 1.0.0
 */
@Data
@EqualsAndHashCode(callSuper = false, onlyExplicitlyIncluded = true)
@ToString(callSuper = false, onlyExplicitlyIncluded = true)
public class Session extends User {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;

	public Session(Usuario usuario, Collection<? extends GrantedAuthority> authorities) {
		super(usuario.getNombreUsuario(), 
				usuario.getClaveUsuario(), 
				usuario.getEstadoEntidad(), 
				true, 
				true, 
				true,
				authorities);
		this.usuario = usuario;
	}
}
