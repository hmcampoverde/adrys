package org.hmcampoverde.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@MappedSuperclass
public class Entidad implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	@Column(name = "estado_entidad", nullable = false, columnDefinition = "BOOLEAN DEFAULT 'TRUE'", insertable = false)
	private Boolean estadoEntidad;

}
