package org.hmcampoverde.servicio;

import java.util.List;

import org.hmcampoverde.modelo.Menu;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
public interface MenuServicio extends Servicio<Menu> {

	public List<Menu> obtenerPorRol(String nombreRol);

}
