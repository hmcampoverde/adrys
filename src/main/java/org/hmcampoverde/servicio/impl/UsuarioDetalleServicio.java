package org.hmcampoverde.servicio.impl;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import org.hmcampoverde.dao.UsuarioDao;
import org.hmcampoverde.modelo.Rol;
import org.hmcampoverde.modelo.Session;
import org.hmcampoverde.modelo.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
public class UsuarioDetalleServicio implements UserDetailsService {

	@Autowired
	private UsuarioDao usuarioDao;

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String nombreUsuario) throws UsernameNotFoundException {
		Usuario usuario = usuarioDao
				.obtenerPorNombreUsuario(nombreUsuario)
				.orElseThrow(() -> new UsernameNotFoundException("Login Username Invalido."));
		return new Session(usuario, getRoles(usuario));
	}

	private Collection<? extends GrantedAuthority> getRoles(Usuario usuario) {
		Set<SimpleGrantedAuthority> roles = new HashSet<>();
		usuario.getRoles()
				.forEach((Rol rol) -> roles.add(new SimpleGrantedAuthority("ROLE_".concat(rol.getNombreRol().toUpperCase()))));
		return roles;
	}
}
