package org.hmcampoverde.servicio.impl;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.hmcampoverde.dao.MenuDao;
import org.hmcampoverde.excepcion.NegocioExcepcion;
import org.hmcampoverde.modelo.Menu;
import org.hmcampoverde.servicio.MenuServicio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
public class MenuServicioImpl implements MenuServicio {

	@Autowired
	private MenuDao menuDao;

	@Override
	public List<Menu> obtener() {
		Menu menu = new Menu();
		menu.setEstadoEntidad(Boolean.TRUE);

		Example<Menu> menuExample = Example.of(menu);
		return menuDao.findAll(menuExample, Sort.by(Sort.Direction.ASC, "nombreMenu"));
	}

	@Override
	public Optional<Menu> obtener(String nombre) {
		Menu menu = new Menu();
		menu.setNombreMenu(nombre);
		menu.setEstadoEntidad(Boolean.TRUE);

		Example<Menu> menuExample = Example.of(menu);
		return menuDao.findOne(menuExample);
	}

	@Override
	public Optional<Menu> obtener(Integer id) {
		return menuDao.findById(id);
	}

	@Override
	public Menu guardar(Menu entidad) {
		if (existe(entidad)) {
			throw new NegocioExcepcion("canton.mensaje.existeCanton");
		}
		return menuDao.save(entidad);
	}

	@Override
	public Menu eliminar(Menu entidad) {
		entidad.setEstadoEntidad(Boolean.FALSE);
		return menuDao.save(entidad);
	}

	@Override
	public List<Menu> eliminar(List<Menu> registros) {
		registros.forEach(menu -> menu.setEstadoEntidad(Boolean.FALSE));
		return menuDao.saveAll(registros);
	}

	@Override
	public boolean existe(Menu entidad) {
		return obtener(entidad.getNombreMenu())
				.map(canton -> Objects.nonNull(canton) && !canton.getIdMenu().equals(entidad.getIdMenu()))
				.orElse(Boolean.FALSE);
	}

	@Override
	public List<Menu> obtenerPorRol(String nombreRol) {
		return menuDao.obtenerPorRol(nombreRol);
	}
}
