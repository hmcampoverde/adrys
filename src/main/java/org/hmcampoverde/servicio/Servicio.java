package org.hmcampoverde.servicio;

import java.util.List;
import java.util.Optional;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
public interface Servicio<T> {

	public List<T> obtener();

	public Optional<T> obtener(String propiedad);

	public Optional<T> obtener(Integer id);

	public T guardar(T entidad);

	public T eliminar(T entidad);

	public List<T> eliminar(List<T> registros);

	public boolean existe(T entidad);
}
