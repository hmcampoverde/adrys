package org.hmcampoverde;

import org.hmcampoverde.servicio.impl.UsuarioDetalleServicio;
import org.hmcampoverde.spring.security.AccesoDenegado;
import org.hmcampoverde.spring.security.Redireccionamiento;
import org.hmcampoverde.spring.security.SolicitudAutenticacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SeguridadAplicacion extends WebSecurityConfigurerAdapter  {
	
	String[] resources = new String[] { "/resources/**", "/javax.faces.resource/**", "/error/**", };

	@Autowired
	private UsuarioDetalleServicio usuarioDetalleServicio;
	
	@Autowired
	private AccesoDenegado accesoDenegado;

	@Autowired
	private Redireccionamiento redireccionamiento;

	@Autowired
	private SolicitudAutenticacion autenticacion;

	@Autowired
	private BCryptPasswordEncoder codificadorClave;


	@Override
	protected void configure(HttpSecurity http) throws Exception {
		autenticacion.setRedirectStrategy(redireccionamiento);
		http.authorizeRequests()
			.antMatchers(resources)
				.permitAll()
			.antMatchers("/administracion/empleado/**")
				.hasAnyRole("TICS")
			.antMatchers("/administracion/soporte/**")
				.hasAnyRole("TICS")
			.antMatchers("/administracion/unidad/**")
				.hasAnyRole("TICS")
			.antMatchers("/administracion/admin/**")
				.hasAnyRole("TICS")
			.antMatchers("/", "/index.xhtml", "/login.xhtml")
				.permitAll()
			.anyRequest()
				.authenticated()
			.and()
				.formLogin()
				.loginPage("/login.xhtml")
				.defaultSuccessUrl("/administracion/index.xhtml")
				.failureUrl("/login.xhtml?error=true")
				.loginProcessingUrl("/login.xhtml")
			.and()
				.csrf()
					.disable()
				.logout()
					.logoutUrl("/j_spring_security_logout")
					.invalidateHttpSession(Boolean.TRUE)
					.deleteCookies("JSESSIONID")
				.logoutSuccessUrl("/login.xhtml?logout")
			.and()
				.sessionManagement()
				.invalidSessionUrl("/error/session-expirada.xhtml")
				.sessionAuthenticationErrorUrl("/error/session-expirada.xhtml")
			.and()
				.headers()
				.frameOptions()
				.sameOrigin()
			.and()
			.exceptionHandling()
			.authenticationEntryPoint(autenticacion)
			.accessDeniedHandler(accesoDenegado);

	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(usuarioDetalleServicio).passwordEncoder(codificadorClave);
	}

	@Bean
	public BCryptPasswordEncoder codificadorClave() {
		return new BCryptPasswordEncoder();
	}
}
