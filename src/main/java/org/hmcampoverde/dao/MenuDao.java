package org.hmcampoverde.dao;

import org.hmcampoverde.modelo.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface MenuDao extends JpaRepository<Menu, Integer>, MenuDaoP {

}
