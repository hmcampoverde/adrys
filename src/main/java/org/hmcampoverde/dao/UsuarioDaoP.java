package org.hmcampoverde.dao;

import java.util.Optional;

import org.hmcampoverde.modelo.Usuario;
import org.springframework.stereotype.Repository;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface UsuarioDaoP {

	Optional<Usuario> obtenerPorNombreUsuario(String usuario);

}
