package org.hmcampoverde.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hmcampoverde.dao.MenuDaoP;
import org.hmcampoverde.modelo.Menu;
import org.hmcampoverde.modelo.Rol;
import org.springframework.transaction.annotation.Transactional;

public class MenuDaoPImpl implements MenuDaoP {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = true)
	public List<Menu> obtenerPorRol(String nombreRol) {
		List<Predicate> predicates = new ArrayList<>();
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Menu> query = builder.createQuery(Menu.class);
		Root<Menu> root = query.from(Menu.class);
		Join<Menu, Rol> join = root.join("roles");
		predicates.add(builder.isTrue(root.get("estadoEntidad")));
		predicates.add(builder.equal(join.get("nombreRol"), nombreRol));
		query.where(builder.and(predicates.toArray(new Predicate[predicates.size()])));
		query.orderBy(builder.desc(root.get("nombreMenu")));
		return entityManager.createQuery(query).getResultList();
	}

}
