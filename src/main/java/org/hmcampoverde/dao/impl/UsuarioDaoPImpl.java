package org.hmcampoverde.dao.impl;

import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;

import org.hmcampoverde.dao.UsuarioDaoP;
import org.hmcampoverde.modelo.Usuario;
import org.springframework.transaction.annotation.Transactional;

public class UsuarioDaoPImpl implements UsuarioDaoP {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional(readOnly = true)
	public Optional<Usuario> obtenerPorNombreUsuario(String usuario) {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Usuario> query = builder.createQuery(Usuario.class);
		Root<Usuario> root = query.from(Usuario.class);
		root.fetch("roles",JoinType.LEFT);
		query.where(builder.equal(root.get("nombreUsuario"), usuario));
		return Optional.ofNullable(entityManager.createQuery(query).getSingleResult());
	}

}
