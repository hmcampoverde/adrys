package org.hmcampoverde.dao;

import java.util.List;

import org.hmcampoverde.modelo.Menu;
import org.springframework.stereotype.Repository;

/**
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface MenuDaoP {

	List<Menu> obtenerPorRol(String nombreRol);
}
