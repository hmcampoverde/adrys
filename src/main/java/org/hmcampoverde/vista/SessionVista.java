package org.hmcampoverde.vista;

import java.io.Serializable;
import java.util.Objects;
import java.util.ResourceBundle;

import lombok.Getter;
import lombok.Setter;

import org.hmcampoverde.modelo.Session;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 * @author Henry Manuel Campoverde Hidalgo.
 * @version 1.0.0
 * @since 1.0.0
 */
@Named
@ViewScoped
public class SessionVista implements Serializable {

	private static final long serialVersionUID = 1L;

	@Getter
	@Setter
	private Session session;

	@Getter
	@Setter
	private String descripcionSession;

	@Getter
	@Setter
	private boolean estaAutenticado = Boolean.FALSE;

	public SessionVista() {
		Authentication autenticacion = SecurityContextHolder.getContext().getAuthentication();
		Object sessionUser = autenticacion.getPrincipal();

		if (autenticacion != null && autenticacion.isAuthenticated()
				&& !(autenticacion instanceof AnonymousAuthenticationToken)) {
			estaAutenticado = Boolean.TRUE;
		}

		if (Objects.nonNull(autenticacion) && Objects.nonNull(sessionUser)) {
			if (sessionUser instanceof String && (autenticacion instanceof AnonymousAuthenticationToken)) {
				descripcionSession = ResourceBundle.getBundle("pagina").getString("app.usuario.anonimo");
			} else if (sessionUser instanceof Session) {
				session = ((Session) sessionUser);
				descripcionSession = session.getUsuario().getDescripcionUsuario();
			}
		}
	}

	public boolean authenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return (authentication != null && authentication.isAuthenticated()
				&& !(authentication instanceof AnonymousAuthenticationToken));
	}
}
