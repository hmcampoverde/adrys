package org.hmcampoverde.vista;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;

import javax.faces.view.ViewScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

@Named
@ViewScoped
public class NavegacionVista implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final String PAGINA_PRINCIPAL = "/administracion/empty";

	@Getter
	@Setter
	private String enlacePagina;

	@Getter
	@Setter
	private String nombrePagina;

	public NavegacionVista() {
		enlacePagina = PAGINA_PRINCIPAL;
		nombrePagina = ResourceBundle.getBundle("pagina").getString("dashboard.titulo");
	}

	public void navegarAPagina(String nombrePagina, String enlacePagina, boolean paginaExterna) {
		if (paginaExterna) {
			FacesContext context = FacesContext.getCurrentInstance();
			Map<String, Object> viewMap = context.getViewRoot().getViewMap();
			List<String> viewMapKey = new ArrayList<>();
			for (String key : viewMap.keySet()) {
				if (!key.equals("navegacionControlador")) {
					viewMapKey.add(key);
				}
			}

			if (viewMapKey != null && !viewMapKey.isEmpty()) {
				for (String key : viewMapKey) {
					viewMap.remove(key);
				}
			}
		}

		this.nombrePagina = nombrePagina;
		this.enlacePagina = enlacePagina;

	}
}
