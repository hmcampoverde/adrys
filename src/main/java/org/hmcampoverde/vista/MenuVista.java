package org.hmcampoverde.vista;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.annotation.PostConstruct;

import org.hmcampoverde.modelo.Menu;
import org.hmcampoverde.modelo.Rol;
import org.hmcampoverde.servicio.MenuServicio;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSeparator;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;

@Named
@SessionScoped
public class MenuVista implements Serializable {

	private static final long serialVersionUID = 1L;

	@Autowired
	private MenuServicio menuServicio;

	@Autowired
	private SessionVista sessionControlador;

	@Getter
	@Setter
	private MenuModel menuModel;

	@Getter
	@Setter
	private List<Menu> menus;

	@PostConstruct
	public void postConstructor() {
		obtenerMenu();
		crearMenuModel(null, null);
		agregarSeparadorSubMenu();
	}

	public void obtenerMenu() {
		menus = new ArrayList<>();
		menuModel = new DefaultMenuModel();

		if (Objects.nonNull(menus)) {
			sessionControlador.getSession().getUsuario().getRoles()
					.forEach((Rol rol) -> menus.addAll(menuServicio.obtenerPorRol(rol.getNombreRol())));
		}
	}

	private void crearMenuModel(Menu menu, DefaultSubMenu defaultSubmenu) {
		menus.forEach((Menu menuItem) -> {
			if (Objects.isNull(menu)) {
				if (Objects.isNull(menuItem.getSubmenu())) {
					if (tieneItems(menuItem)) {
						DefaultSubMenu defaultSubmenuTemp = DefaultSubMenu.builder().label(menuItem.getNombreMenu())
								.icon(menuItem.getIconoMenu()).build();
						crearMenuModel(menuItem, defaultSubmenuTemp);
						menuModel.getElements().add(defaultSubmenuTemp);
					} else {
						if (Objects.nonNull(menuItem.getUrlMenu())) {
							DefaultMenuItem defaultMenuItem = DefaultMenuItem.builder().value(menuItem.getNombreMenu())
									.icon(menuItem.getIconoMenu())
									.command("#{navegacionControlador.".concat("navegarAPagina('")
											.concat(menuItem.getNombreMenu()).concat("','")
											.concat(menuItem.getUrlMenu()).concat("',").concat("true").concat(")}"))
									.update(":panel-principal :titulo").onstart("NProgress.start();")
									.onsuccess("NProgress.done();").build();
							menuModel.getElements().add(defaultMenuItem);
						}
					}
				}
			} else {
				if (Objects.nonNull(menuItem.getSubmenu()) && menuItem.getSubmenu().equals(menu)) {
					if (tieneItems(menuItem)) {
						DefaultSubMenu defaultSubmenuTemp = DefaultSubMenu.builder().label(menuItem.getNombreMenu())
								.icon(menuItem.getIconoMenu()).build();
						crearMenuModel(menuItem, defaultSubmenuTemp);
						defaultSubmenu.getElements().add(defaultSubmenuTemp);
					} else {
						if (Objects.nonNull(menuItem.getUrlMenu())) {
							DefaultMenuItem defaultMenuItem = DefaultMenuItem.builder().value(menuItem.getNombreMenu())
									.icon(menuItem.getIconoMenu())
									.command("#{navegacionControlador.".concat("navegarAPagina('")
											.concat(menuItem.getNombreMenu()).concat("','")
											.concat(menuItem.getUrlMenu()).concat("',").concat("true").concat(")}"))
									.update(":panel-principal :titulo").onstart("NProgress.start();")
									.onsuccess("NProgress.done();").build();
							defaultSubmenu.getElements().add(defaultMenuItem);
						}
					}
				}
			}
		});
	}

	private boolean tieneItems(Menu menu) {
		return (menus.stream()
				.filter((Menu menuItem) -> Objects.nonNull(menuItem.getSubmenu()) && menuItem.getSubmenu().equals(menu))
				.count() > 0);
	}

	private void agregarSeparadorSubMenu() {
		menuModel.getElements().forEach((MenuElement menuElement) -> {
			if (menuElement instanceof DefaultSubMenu) {
				((DefaultSubMenu) menuElement).getElements().add(new DefaultSeparator());
			}
		});
	}
}
