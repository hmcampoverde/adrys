package org.hmcampoverde.excepcion;

import java.util.Map;
import javax.faces.application.ViewExpiredException;
import javax.faces.context.ExceptionHandler;
import org.omnifaces.util.Exceptions;
import org.primefaces.application.exceptionhandler.PrimeExceptionHandler;
import org.springframework.security.access.AccessDeniedException;

/**
 * Administrador de Excepciones del Aplicativo Web.
 *
 * @author Henry Manuel Campoverde Hidalgo
 * @version 1.0.0
 * @since 1.0.0
 */
public class Excepcion extends PrimeExceptionHandler {

	private static final String PAGINA_ERROR = "/error/error.xhtml";
	private static final String PAGINA_SESSION_EXPIRADA = "/error/sesion.xhtml";
	private static final String PAGINA_ACCESO_DENEGADO = "/error/acceso.xhtml";

	public Excepcion(ExceptionHandler wrapped) {
		super(wrapped);
	}

	@Override
	protected String evaluateErrorPage(Map<String, String> errorPages, Throwable rootCause) {
		if (Exceptions.is(rootCause, ViewExpiredException.class)) {
			return PAGINA_SESSION_EXPIRADA;
		} else if (Exceptions.is(rootCause, AccessDeniedException.class)) {
			return PAGINA_ACCESO_DENEGADO;
		} else {
			return PAGINA_ERROR;
		}
	}
}
