package org.hmcampoverde.excepcion;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;
import org.primefaces.application.exceptionhandler.PrimeExceptionHandlerFactory;

public class ExcepcionFactory extends PrimeExceptionHandlerFactory {

	public ExcepcionFactory(final ExceptionHandlerFactory wrapped) {
		super(wrapped);
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return new Excepcion(getWrapped().getExceptionHandler());
	}
}
